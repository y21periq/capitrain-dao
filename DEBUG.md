Nous avons exploré le code comme indiqué dans les notes, càd :
- execution_experiment
- experiment_controller
- concerto_d_g5k

Après avoir fait le tour en parcourant les différents scénarios d'appels entre les fonctions des fichiers,
nous avons essayé de démarrer une expérience depuis G5K. Cependant, avant de pouvoir lancer le projet, 
il nous a fallu corriger quelques erreurs sur les imports (les imports font références à des packages qui n'existe pas,
peut être le fichier init est manquant). Nous avons donc juste appelé les fichiers sans passer par les packages.

Par la suite, nous avons essayé de démarrer l'application en partant du fichier `execution_experiment.py`.
D'après son main, il nécessite un fichier pour être démarré qui se trouve être le fichier de configuration.
Nous avons donc trouvé intéressant d'utiliser le fichier `expe_parameters_example.yml` pour exécuter une 
expérience d'exemple.
Pour run l'expérience, nous avons run cette commande :

```shell
python3 experiment/execution_experiment.py expe_parameters_example.yaml
```

Nous avons alors pu observer la réservation des nœuds puis une erreur est survenu par rapport à un dossier manquant.
Cette erreur était dû à la configuration par défaut définissant le home des projets concerto comme : `/home/user/concerto-d-projects"`
Nous avons donc changé cela par `/home/yperiquoi/concerto-d-projects"`.

Par la suite lors de lancement la réservation des nœuds ainsi que le lancement des expériences a eu lieu, mais lors
de l'exécution des expériences une erreur sur la récupération des logs a lieu (comme expliqué dans le mail)
Avec le run de la commande :

```shell
python3 experiment/execution_experiment.py expe_parameters_example.yaml
```

On obtient la trace suivante :

```
Start openstack-deployment
Reservation with the following parameters:
job_name_concerto: concerto-d
walltime: 01:00:00
reservation:
nb_server_clients: 0
nb_servers: 1
nb_dependencies: 1
nb_zenoh_routers: 1
cluster: grisou
Check host keys for nodes in nancy
Check grisou-21.nancy.grid5000.fr
# Host grisou-21.nancy.grid5000.fr found: line 52
grisou-21.nancy.grid5000.fr ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7phYyfVGwRjeAf08dLHcEV1Lz2IL0KfMD+oWOVrmqaRvdtAkxCb9g3Xw3kV6THbdsotz2qTFvwpUUSoNhXCjFkdfNSjlgWCeR+WEP7yLmeV5N8ijhpX8RaZjMvbRYlmgTv36Sfvn0CzoH6ZKsMaMmiHpksKt8BURmn938pNxW7NIz/7tTVBpyTp17UK0jvzVuXFou13HIa3g0h6qlHgLG2ZmB1PmiUs1GofbqFprtOfP3SN37KxryozQs7jd4TPBDwUa2p6GMI/EKOyHM/RC/XM7GU0O/wGUpXt3ogthJrD/v8GRYUeh8DVyk5BbOoDj9ykTQLmdQ0FaF5ZxfQGd/
# Host grisou-21.nancy.grid5000.fr found: line 53
grisou-21.nancy.grid5000.fr ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBJNRaXwelZ7FQiXGmZ30bNRvetHTtEy0ibJ8BPIDwV0NsGzgskvX5jMfwNrQh2aGMq1RJinsaLP2VzStGJ/ft6U=
# Host grisou-21.nancy.grid5000.fr found: line 54
grisou-21.nancy.grid5000.fr ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDtmdiDXatysqOMrTX6u7NLenLFDMcoEY8p9V98nKCt3
Check grisou-29.nancy.grid5000.fr
# Host grisou-29.nancy.grid5000.fr found: line 45
grisou-29.nancy.grid5000.fr ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6Qp+z5EHlCV1+AXElq/A7Wy6+yGQHLoqS19M9fp1BYj12WCbfWE00hLdhtLKTOdnX0jU1YEGO+HtqYUEw4iVA0IBQh6YGRQ0Rbf8COibykwVNnza1BfR8/ilDZKy4jVodSoPCoOn8MeOD8+CVVnRIU0kFR1MTcUhWKByCzUFDznTbUpHfYSVJAWsy3o1R4S6cWOZGDsOb5m/XJ+y/bokw6HsROLepjdsM9ZCVc9GE+5r4CWbctsMIanQQRQoh3xQfUMUADligGijwptAcWXY80KR2mGWyi8AgNgu1lgBDDR3WQsEWGeOlQVV0CN9YXzs83ICnINAnaS7U3CJG/0PR
# Host grisou-29.nancy.grid5000.fr found: line 46
grisou-29.nancy.grid5000.fr ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBGDDD7rkRyMGuWSE1amAuNsZ9iCzOeP4FNZn694+0FkYTYshqMCuQCUgmfjBADoTXG1TJ2+a20t/LOdbq3EPdbg=
# Host grisou-29.nancy.grid5000.fr found: line 47
grisou-29.nancy.grid5000.fr ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBgiNjKINkg6x9/H1l5JLYsvhkLkTC9is4lQ0bGBkslf
Check grisou-3.nancy.grid5000.fr
# Host grisou-3.nancy.grid5000.fr found: line 55
grisou-3.nancy.grid5000.fr ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBCBTJAAsQEzz57w/H03IH3Vi4l398VjVKC56PYTzxQCqVDnkdKJByJuKXKlbi6U63BY7Rkxw2KZvdSet8wZ9bJw=
# Host grisou-3.nancy.grid5000.fr found: line 56
grisou-3.nancy.grid5000.fr ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCetcnMx3gEYnigXPo8BMXE/N+w4dwlyPg8sSt6/9TBaw1WP+Mnxrdcyhqjt6trm9WS6gOB6YbTpZdCAQRwwPu7YPCW0noHta5fZmoi/UHTMKtrLZH4Y3mvfMCjJ7RiJCl94257DA2h2TyyMWTRDz+OgNvgZaUzhrDYcUpuIN/MYxwmpJP48YxslL9R3a8uQgBBfuvSdLPGgaTIAe368Be5MsNSYT2CDo/5W8JsLsHEauuyw7dgpkNjnGqtUODIYamWdv5GZhdYfZWqxOnvUc+WJmQLMLMZu403VIZkZ+hmJY/D4yEZmbf8awh+7KBpzYpLqQTMzL4IMt6GfKC7IY7/
# Host grisou-3.nancy.grid5000.fr found: line 57
grisou-3.nancy.grid5000.fr ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICYDpxH2SbOkKRQrZoZBSDP4ouRcYvjm9wmfLVa9dfQh
Reserved roles:
server: grisou-21.nancy.grid5000.fr
dep0: grisou-29.nancy.grid5000.fr
zenoh_routers: grisou-3.nancy.grid5000.fr
Initialise repositories
[WARNING]: No inventory was parsed, only implicit localhost is available
Finished 3 tasks (apt,git,pip)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
Global expe dir: /home/yperiquoi/concerto-d-projects/experiment-openstack-deployment-dir
Number of experiments: 6
sweeps <6 total, 0 done, 0 skipped, 0 in progress, 6 remaining>
----- All experiments parameters -----
{'uptimes': ['mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json', 'mascots_uptimes-60-50-5-ud1_od0_15_25_perc.json', 'mascots_uptimes-60-50-5-ud2_od0_15_25_perc.json'], 'transitions_times': ['transitions_times-1-30-deps12-0.json', 'transitions_times-1-30-deps12-1.json'], 'waiting_rate': [1], 'id': [1]}
--------------------------------------
------------------------ Execution of experiments start ------------------------
----- Starting experiment ---------
-- Expe parameters --
Uptimes: mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json
Transitions times: transitions_times-1-30-deps12-0.json
Waiting rate: 1
Id: 1
---------------------
-------------- Initialising dirs ---------------
------------ Expe dir: /home/yperiquoi/concerto-d-projects/experiment-openstack-deployment-dir/results_openstack-deployment_asynchronous_grisou_T0_mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json_waiting_rate-1-2023-11-21_15-04-59 ---------------------
------------ Execution dir: /root/concerto-d-projects/experiment-openstack-deployment-dir/results_openstack-deployment_asynchronous_grisou_T0_mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json_waiting_rate-1-2023-11-21_15-04-59 ---------------------
-------------------- Launching experiment -----------------------
Finished 1 tasks (file)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
Put inventory file on each
Finished 1 tasks (copy)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
------- Deploy zenoh routers -------
Finished 1 tasks (file)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
Finished 1 tasks (copy)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
Finished 2 tasks (apt,unarchive)
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
launch zenoh routers with 11617 timeout
Finished 1 tasks (kill $(ps -ef | grep -v grep | grep -w '/root/concerto-d-projects/zenoh_install/zenohd -c /root/concerto-d-projects/zenoh_install/zenohd-config.json5' | awk '{print $2}'))
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
Finished 1 tasks (timeout 11617 /root/concerto-d-projects/zenoh_install/zenohd -c /root/concerto-d-projects/zenoh_install/zenohd-config.json5 --cfg 'plugins_search_dirs:["/root/concerto-d-projects/zenoh_install"]')
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
------- Run experiment ----------
SCHEDULING START
Controller 0 sleep for 54.99545931816101
Controller 1 skip round 0, no uptime
Round reconf for dep0: 1
Controller 1 sleep for 194.99390316009521
Start execution reconfiguration, command executed: cd /root/concerto-d-projects/concerto-decentralized; export PYTHONPATH=$PYTHONPATH:/root/concerto-d-projects/evaluation; /root/concerto-d-projects/concerto-decentralized/venv/bin/python3 /root/concerto-d-projects/evaluation/synthetic_use_case/parallel_deps/reconf_programs/reconf_server.py /root/concerto-d-projects/experiment_files/parameters/transitions_times/transitions_times-1-30-deps12-0.json 50 1 2023-11-21_15-06-13 /root/concerto-d-projects/experiment-openstack-deployment-dir/results_openstack-deployment_asynchronous_grisou_T0_mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json_waiting_rate-1-2023-11-21_15-04-59 asynchronous deploy 1 --debug_current_uptime_and_overlap "--- FREQ: 0 -----
[55, 50][-1, 50]     Server/dep0   0      None
[55, 50][0, 50]      Server/dep1   0      Left
[55, 50][110, 50]    Server/dep2   0      Right
[55, 50][92.5, 50]   Server/dep3   12.5   Right
[55, 50][110, 50]    Server/dep4   0      Right
-----------------
"
Exit code: 1 for server
scp: /root/concerto-d-projects/experiment-openstack-deployment-dir/results_openstack-deployment_asynchronous_grisou_T0_mascots_uptimes-60-50-5-ud0_od0_15_25_perc.json_waiting_rate-1-2023-11-21_15-04-59/logs/logs_server.txt: No such file or directory
 
```

De ce que nous avons compris cette erreur est lié au manque de l'écriture du fichier de résultat dans le dossier des logs.
Malgrès quelques modifications (à voir avec git diff) nous n'avons pas réussi à aller plus loins car cette erreur nous bloque et n'est pas très parlante.

Nous avons essayé de créer manuellement le fichier de logs pour s'assurer que le système de log puisse écrit dedans, essayer plusieurs fois de changer
les chemins d'accès aux logs pour être sûr que les chemins ne soit juste pas erroné, mais il semble juste que celui-ci ne soit pas créé.

### Le seul test utilisé est donc celui décrit dans le fichier `expe_parameters_example.py`

### Aucun fichier d'entrée n'est donc utilisé explicitement à par ceux décrit dans l'expérience d'exemple.